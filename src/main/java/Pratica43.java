/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import utfpr.ct.dainf.if62c.pratica.Quadrado;
import utfpr.ct.dainf.if62c.pratica.Retangulo;
import utfpr.ct.dainf.if62c.pratica.TrianguloEquilatero;


/**
 *
 * @author zacaron
 */
public class Pratica43 {
    
    public static void main(String[] args) {
        
        Quadrado q = new Quadrado(4);
        Retangulo r = new Retangulo(4,2);
        TrianguloEquilatero t = new TrianguloEquilatero(4);
        
        //Área e Perímetro
        System.out.println("Area do " + q + " = " + q.getArea());
        System.out.println("Perímetro do " + q + " = " + q.getPerimetro());
        System.out.println("Area do " + r + " = " + r.getArea());
        System.out.println("Perímetro do " + r + " = " + r.getPerimetro());
        System.out.println("Area do " + t + " = " + t.getArea());
        System.out.println("Perímetro do " + t + " = " + t.getPerimetro());
        
        
        //LadoMaior e Menor
        System.out.println("Lado Maior do " + q + " = " + q.getLadoMaior());
        System.out.println("Lado Menor do " + q + " = " + q.getLadoMenor());
        System.out.println("Lado Maior do " + r + " = " + r.getLadoMaior());
        System.out.println("Lado Menor do " + r + " = " + r.getLadoMenor());
        System.out.println("Lado Maior do " + t + " = " + t.getLadoMaior());
        System.out.println("Lado Menor do " + t + " = " + t.getLadoMenor());
        
    
     
        
        
        
        
        
    }
    
}
