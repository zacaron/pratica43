/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author zacaron
 */
public interface FiguraComEixos extends Figura{
    
    //public double eixoMenor;
    //public double eixoMaior;
     
    
    double getEixoMenor();
    double getEixoMaior();
    

}
    
    
