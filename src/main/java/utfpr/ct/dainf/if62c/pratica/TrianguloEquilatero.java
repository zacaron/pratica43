/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author zacaron
 */
public class TrianguloEquilatero extends Retangulo {
    
    public TrianguloEquilatero(double r) {
        super(r);
    }
    
    @Override
    public double getArea() {
        return (Math.pow(getR(), 2)*Math.sqrt(3))/4;
    }
  
    @Override
    public double getPerimetro() {
        return getR()*3;
    }
    
}
