/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author zacaron
 */
public class Circulo extends Elipse {
    public Circulo (){
        super();
    }
    
    public Circulo(double eixoMenor){
        super(eixoMenor,eixoMenor);
    }
    
    @Override
    public double getPerimetro() {
        return 2 * Math.PI*getEixoMenor();
    }
    
    
}
