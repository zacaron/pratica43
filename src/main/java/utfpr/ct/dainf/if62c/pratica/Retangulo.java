/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author zacaron
 */
public class Retangulo implements FiguraComLados {
    
    @Override
    public String getNome() {
    return this.getClass().getSimpleName();
    }
    
    private double r;
    private double s;
    private double t;
    
    public Retangulo(double r, double s) {
        this.r = r;
        this.s = s;
    }
    
    public Retangulo(double r) {
        this.r = r;
        this.s = r;
        this.t = r;
    }
    
    @Override
    public double getLadoMenor() {
        return this.r < this.s ? this.r : this.s;
    }

    @Override
    public double getLadoMaior() {
        return this.r > this.s ? this.r : this.s;
    }

    /*@Override
    public String getNome() {
        return "Retângulo";
    }*/

    @Override
    public double getPerimetro() {
        return (this.r*2)+(this.s*2);
    }

    @Override
    public double getArea() {
        return this.r*this.s;
    }


    public double getR() {
        return r;
    }

    public void setR(double r) {
        this.r = r;
    }

 
    public double getS() {
        return s;
    }


    public void setS(double s) {
        this.s = s;
    }


    public double getT() {
        return t;
    }


    public void setT(double t) {
        this.t = t;
    }
    
    @Override
    public String toString() {
        return getNome();
    }
    
}
