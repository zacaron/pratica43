/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author zacaron
 */
public class Elipse implements FiguraComEixos {
    
public String getNome() {
    return this.getClass().getSimpleName();
    }
    
    private double r;
    private double s;
            
    public Elipse(){
        r=s=0;
    }
    public Elipse(double r, double s) {
        this.r = r;
        this.s = s;
    }

    public double getR() {
        return r;
    }

    public void setR(double r) {
        this.r = r;
    }

    public double getS() {
        return s;
    }

    public void setS(double s) {
        this.s = s;
    }
    @Override
    public double getEixoMenor(){
        return this.r < this.s?this.r:this.s;
    }
    
    @Override
     public double getEixoMaior(){
        return this.r > this.s?this.r:this.s;
    }
        
    @Override
    public double getArea() {
        return Math.PI*r*s;
    }
    
    @Override
    public double getPerimetro() {
        return Math.PI*(3*(r+s)-Math.sqrt((3*r+s)*(r+3*s)));
        
    }
    
    @Override
    public String toString() {
        return getNome();
    }
    
}
